package cn.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Author it
 * @Description TODO
 * @Date 2022/6/21 15:24
 * @Version 1.0
 */
@EnableEurekaServer
@SpringBootApplication
public class CommonEurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommonEurekaApplication.class, args);
    }
}
